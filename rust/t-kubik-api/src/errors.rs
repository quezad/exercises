use std::fmt;
use bcrypt::BcryptError;
use diesel::result;

#[derive(Debug)]
pub enum TKubikError{
    HashError(BcryptError),
    DBError(result::Error),
    PasswordNotMatch(String),
    WrongPassword(String)
}

impl From<BcryptError> for TKubikError{
    fn from(error: BcryptError) -> Self{
        TKubikError::HashError(error)
    }
}
impl From<result::Error> for TKubikError{
    fn from(error: result::Error) -> Self{
        TKubikError::DBError(error)
    }
}

impl fmt::Display for TKubikError{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result{
        match self{
            TKubikError::HashError(error) => write!(f, "{}", error),
            TKubikError::DBError(error) => write!(f, "{}", error),
            TKubikError::PasswordNotMatch(error) => write!(f, "{}", error),
            TKubikError::WrongPassword(error) => write!(f, "{}", error)
        }
    }
}    
