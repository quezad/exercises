package org.mquezada.exercises.stacks;

import org.mquezada.exercises.stacks.CustomEmptyStackException;

// T is generics for any object, it can easily be replaced with e.g. String
public class Stack<T>{
	
	 // Every stack begins with a top (which is null at the beginning).
     private StackNode<T> top;
     // Just a reminder of how many nodes are still on the stack.
     private int size;
     
     public Stack() {
		top = null;
		size = -1;
     }
     
     // First a method to retrieve and remove the top of the stack.
     public T pop(){
     	// We got two options here, check if size is valid (>=0) or there's a null top
     	if(top == null){
     		throw new CustomEmptyStackException("Ooops!");
     	}

     	// If everything went right we should be able to pop and object.

     	// Because this is the content of the node, and we don't care about the envelope.
     	T item = top.getData();

     	// Since we obtained the object, time the top is next in line, and adjust the size.
     	top = top.getNext();
     	size--;

     	// Finally return the object.
     	return item;
     }

     // Next to push things to the stack.
     public void push(T item){
     	// first we need a new node with the data and make it the top.
     	StackNode<T> newTop = new StackNode<T>(item);

     	// This is gonna be the top, and needs to know who's next.
     	newTop.setNext(top);

     	// Finally make it the top, and adjust size.
     	top = newTop;
     	size++;
     }

     // Now check what's next without removing the top.
     public T peek(){
     	// Again, we check of there's actually something on the stack.
     	if(top == null){
     		throw new CustomEmptyStackException("Ooops!");
     	}

     	// That being done, we just return the contents.
     	return top.getData();
     }

     // Just an utility method to check before trying to peek or pop into nothing.
     public boolean isEmpty(){
     	// Directly return the result of the validation.
     	return top == null;
     }

     // Last but not least
     public int size() {
     	// We are working on 0 basis positions.
     	return size + 1;
     }
}
