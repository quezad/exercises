package org.mquezada.exercises.stacks;

public class CustomEmptyStackException extends RuntimeException{
     public CustomEmptyStackException(String errorMessage){
          super(errorMessage);
     }
}
