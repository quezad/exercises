package org.mquezada.exercises.stacks;

import java.util.Scanner;

public class StacksInAction{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		Stack myStack = new Stack<String>();

		String input = "";
		String option = "";
		while (!"q".equalsIgnoreCase(option)) {
		    System.out.println("Enter a command for the stack and hit enter:");
		    System.out.println("(p)ush");
		    System.out.println("p(o)p");
		    System.out.println("p(e)ek");
		    System.out.println("(s)ize");
		    System.out.println("(q)uit");
		    option = scanner.nextLine();

		    switch(option){
		    	case "p":
		    	     System.out.print("Enter a string to push: ");
		    	     input = scanner.nextLine();
		    	     myStack.push(input);
		    	     break;
		    	case "o":
		    	     System.out.println("Popped a: " + myStack.pop());
		    	     break;
		    	case "e":
		    	     System.out.println("I saw a: " + myStack.peek());
			         break;
			    case "s":
			         System.out.println("There is " + myStack.size() + " elements on the stack");
		    	     break;
		    	case "q":
		    	     System.out.println("bye bye!");
		    	     break;
		    	default:
		    	     System.out.println("Invalid command!");

		    }
		  }

		}
}
