package org.mquezada.exercises.stacks;

//This is our basic structure for the stack
public class StackNode<T>{
	// The data inside the node of course
	private T data;

	// A reference to the next node (if any)
	private StackNode<T> next;


	// public constructor so we can use it.
	public StackNode(T data){
		this.data = data;
	}

	// And of course something to acces the data.
	public T getData(){
		return data;
	}

	public StackNode<T> getNext() {
		return next;
	}

	public void setNext(StackNode<T> next) {
		this.next = next;
	}
}