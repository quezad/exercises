package org.mquezada.exercises.tokenizer;

public class Tokenizer {
	
	// Attribute
	String str;
	boolean done;
	
	// Konstruktor
	public Tokenizer(String str) {
		this.str = str;
	}
	
	// Methoden
	
	// Die Methode next(), welche den nächsten Token oder, falls keine Token mehr vorliegen,
	// null zurückgibt.
	
	public String next() { 
		
		String word = getData();
		for (int i = 0; i < str.length(); i++) {
	
			if (str.charAt(i) == ' ') {
				str = str.substring(i+1);
				i= -1;
			}
			else {
				while(str.charAt(i) != ' ');
				word += str.charAt(i);
				i++;
			}
		}
			if (str.length() == 0) {
				done = true;
			}
			return word;
		}
	
	
	public boolean done() {
		// Die Methode done(), welche true zurückgibt, falls keine weiteren Token vorliegen, und
		// false, falls das Gegenteil zutrifft. Der Aufruf dieser Methode darf den internen Zustand
		// der Instanz nicht verändern.
		return done;
	}

	
	public String getData(){
		return str;
	}
}