package org.mquezada.exercises.tokenizer;

public class AlternativeTokenizer {
	private boolean done = false;
	private char[] characters;
	private String[] tokens;
	private int currPosition = 0;
	
	public boolean done() {
		return done;
	}
	
	public AlternativeTokenizer(String sentence) {
		if(sentence == null || sentence.isEmpty()) {
			done = true;
		}
		characters = sentence.toCharArray();
    }
	
	private String copyArrayAndTransform(int begIdx, int endIdx) {
		char[] subarray = new char[endIdx - begIdx + 1];
		for (int i = 0; i < subarray.length; i++) {
			subarray[i] = characters[begIdx + i];
		}
		return String.valueOf(subarray);
	}
	
	private String next() {
		boolean isWord = false;
		int endOfLine = characters.length;
		String nextToken = null;
		
		for(int i = currPosition; i < characters.length; i++) {
			if(!Character.isWhitespace(characters[i]) && i != endOfLine ) {
				isWord = true;
			}else if (Character.isWhitespace(characters[i]) && isWord) {
				isWord = false;
				nextToken = copyArrayAndTransform(currPosition, i-1);
				currPosition = i;
				break;
			}else if(!Character.isWhitespace(characters[i]) && i == endOfLine){
				nextToken = copyArrayAndTransform(currPosition, i-1);
				done = true;
				break;
			}else if(Character.isWhitespace(characters[i]) && !isWord){
				currPosition++;
			}
		}
		
		if(currPosition == endOfLine) {
			done = true;
		}
		
		return nextToken;
	}
	
	public static void main(String[] args) {


		AlternativeTokenizer test = new AlternativeTokenizer(" Das ist    ein   Strin   g  ");
		
		while (!test.done()) {
			System.out.println(test.next());
		}

		


	}
	
}
